/*
 *  Copyright (c) 2011  Marius Cautun
 *
 *                           Kapteyn Astronomical Institute
 *                           University of Groningen, the Netherlands
 *
 *  2016 Boud Roukema - IIinv, IIIinv, Gaussian uncertainties
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <vector>
#include <cmath>
#ifdef OPEN_MP
    #include <omp.h>
#endif
#include <boost/math/special_functions/fpclassify.hpp>


#include "define.h"
#include "particle_data.h"
#include "user_options.h"
#include "quantities.h"
#include "subpartition.h"
#include "miscellaneous.h"
#include "message.h"

#include <gsl/gsl_statistics.h>

using namespace std;
#include "user_options.cc"
#include "quantities.cc"

#include "CIC_interpolation.cc"
#include "TSC_interpolation.cc"
#include "SPH_interpolation.cc"
#include "random.cc"


#ifdef TRIANGULATION
#include "DTFE.h"
#endif


// splits the total data into different computation regions that are done by different threads (if OPEN_MP is enabled)
void DTFE_parallel(vector<Particle_data> *allParticles,
                   vector<Sample_point> &samples,
                   User_options & userOptions,
                   Quantities *uQuantities,
                   Quantities *aQuantities
#ifdef TRIANGULATION
                   , DT &delaunay_triangulation
#endif
                   );

// Computes the velocity divergence, shear and vorticity from the velocity gradient
void computeDivergenceShearVorticity(Field &fields,
                                     int const verboseLevel,
                                     bool const periodic,
                                     std::vector<size_t> grid,
                                     Quantities *quantities);



/* This function interpolates fileds to a grid using the DTFE method.
It takes the following arguments:
        allParticles - stores the particle positions, velocities, scalars (if any) and weights
        samples - stores the position of the grid points where samples are taken (if this grid points don't have a uniform distribution)
        userOptions - options specified by the user plus additional program wise variables
        uQuantities - structure that will store the output quantities - i.e. the fields at the sampling points (NOTE: "uQuantities" stands for "unaveraged quantities" meaning that this fields were NOT averaged over the sampling cell)
        aQuantities - structure that will store the volume averaged output quantities - i.e. the fields volume averaged over the sampling cells (NOTE: "aQuantities" stands for "averaged quantities")

NOTE: This function clears the vector 'allParticles'. */
void DTFE(vector<Particle_data> *allParticles,
          vector<Sample_point> &samples,
          User_options & userOptions,
          Quantities *uQuantities,
          Quantities *aQuantities
#ifdef TRIANGULATION
          , DT &delaunay_triangulation
#endif
          )
{
    //! If the user requested for a set of random particles, generate them
    if ( userOptions.poisson!=0 )
        randomParticles( allParticles, &userOptions );


    //! Select a random subset of the data if the user asked so
    vector<Particle_data> *particlePointer = allParticles; // pointer to the vector containing the particle data
    vector<Particle_data> particlesRandomSubsample;  // vector for storing the random subsample particles
    if (userOptions.randomSample>=Real(0.) )
    {
        randomSample( *particlePointer, &particlesRandomSubsample, userOptions );

        // update 'particlePointer' -> should point to 'particlesRandomSubsample'
        particlePointer->clear();   //clear the particle positions - don't need them anymore
        particlePointer = &particlesRandomSubsample;
    }


    //! Do consistency checks and update some entries in the 'userOptions' structure
    userOptions.updateEntries( particlePointer->size(), not samples.empty() );
    if ( userOptions.averageDensity<0. )
        userOptions.averageDensity = averageDensity( *particlePointer, userOptions );   //computes what is the average density - if not supplied by the user


    // define some variables
    User_options tempOptions = userOptions;
    // the interpolation functions compute only velocity gradient, hence if the user requested the velocity divergence, shear or vorticity, tell the options to compute only the velocity gradient (the rest of the quantities will be computed from the velocity gradient)
    if ( userOptions.uField.selectedVelocityDerivatives() )
    {
        tempOptions.uField.velocity_gradient = true;
        tempOptions.uField.deselectVelocityDerivatives();
    }
    if ( userOptions.aField.selectedVelocityDerivatives() )
    {
        tempOptions.aField.velocity_gradient = true;
        tempOptions.aField.deselectVelocityDerivatives();
    }




    //! Select only the particles in the region of interest
    vector<Particle_data> particlesRegion; // vector for storing the particles in '--region'
    if ( userOptions.regionOn )     // Select only the particles in the user defined region
    {
        //check that the box boundaries satisfy some conditions
        tempOptions.region.validSubBox( tempOptions.boxCoordinates, tempOptions.periodic );

        // find the box values with padding added and find the particles in that extended region
        tempOptions.paddedBox = tempOptions.region;
        tempOptions.paddedBox.addPadding( tempOptions.paddingLength ); // now stores the padded region of interest

        findParticlesInBox( *particlePointer, &particlesRegion, tempOptions );
        tempOptions.periodic = false;   // do not use the periodic translation algorithm after this
        tempOptions.updateFullBox( tempOptions.region );    // now the full box is the region of interest

        // update 'particlePointer' -> should point to 'particles_1'
        particlePointer->clear();   //clear the particle positions - don't need them anymore
        particlePointer = &particlesRegion;
    }

    vector<Particle_data> particlesPartition; // vector for storing the particles in '--partNo' partition
    if ( userOptions.partitionOn and userOptions.partNo>=0 )   // Select only the particles for a user given '--partNo'
    {
        MESSAGE::Message message( userOptions.verboseLevel );
        message << "The program will interpolate the fields in partition number " << userOptions.partNo << " of partition grid [" << MESSAGE::printElements( userOptions.partition, "," ) << "].\n" << MESSAGE::Flush;

                // get the optimal division in a subgrid
                std::vector< std::vector<size_t> > subgridList;
                std::vector< Box > subgridCoords;
                optimalPartitionSplit( *particlePointer, tempOptions, tempOptions.partition, &subgridList, &subgridCoords );
                copySubgridInformation( &tempOptions, subgridList, subgridCoords );     // writes the size of the subgrid to tempOptions.gridSize and the boundaries of the subgrid box to tempOptions.region
        userOptions.region = tempOptions.region;

        // find the box values with padding added and find the particles in that extended region
        tempOptions.paddedBox = tempOptions.region;
        tempOptions.paddedBox.addPadding( tempOptions.paddingLength ); // now stores the padded region of interest

        findParticlesInBox( *particlePointer, &particlesPartition, tempOptions );
        tempOptions.periodic = false;   // do not use the periodic translation algorithm after this

        // update 'particlePointer' -> should point to 'particles_2'
        particlePointer->clear();   //clear the particle positions - don't need them anymore
        particlePointer = &particlesPartition;

        subgrid( userOptions, subgridList );
    }


    //! now compute the DTFE interpolation
    if ( userOptions.partitionOn and userOptions.partNo<0 ) // if option '--partition' was defined (without option '--partNo')
    {
#if NO_DIM==2
        int const totalPartitions = tempOptions.partition[0] * tempOptions.partition[1];
#elif NO_DIM==3
        int const totalPartitions = tempOptions.partition[0] * tempOptions.partition[1] * tempOptions.partition[2];
#endif
        MESSAGE::Message message( userOptions.verboseLevel );
        message << "The program will interpolate the fields in the region of interest using " << totalPartitions << " partitions defined via the grid [" << MESSAGE::printElements( userOptions.partition, "," ) << "].\n" << MESSAGE::Flush;

        // reserve memory for the quantities of interest
        uQuantities->reserveMemory( &(tempOptions.gridSize[0]), tempOptions.uField );
        aQuantities->reserveMemory( &(tempOptions.gridSize[0]), tempOptions.aField );


                // get the optimal division in a subgrid
                std::vector< std::vector<size_t> > subgridList;
                std::vector< Box > subgridCoords;
                optimalPartitionSplit( *particlePointer, tempOptions, tempOptions.partition, &subgridList, &subgridCoords );


        for (int i=0; i<totalPartitions; ++i)
        {
            message << "\n<<< Interpolating the fields for partition " << i+1 << " ...\n";

            // define a temporary 'User_options' object to send values to the DTFE computing function
            User_options tempOpt = tempOptions;
            tempOpt.partNo = i;
                        copySubgridInformation( &tempOpt, subgridList, subgridCoords ); // writes the size of the subgrid to tempOptions.gridSize and the boundaries of the subgrid box to tempOptions.region


            // find the box values with padding added and find the particles in that extended region
            tempOpt.paddedBox = tempOpt.region;
            tempOpt.paddedBox.addPadding( tempOpt.paddingLength ); // now stores the padded region of interest

            vector<Particle_data> tempPart;
            findParticlesInBox( *particlePointer, &tempPart, tempOpt );
            tempOpt.periodic = false;   // do not use the periodic translation algorithm in the "triangulation_interpolation.cpp" file


            //compute the DTFE interpolation
            Quantities temp_uQuantities, temp_aQuantities; // temporary variable to store the grid interpolation for each subgrid
            DTFE_parallel( &tempPart, samples, tempOpt, &temp_uQuantities, &temp_aQuantities
#ifdef TRIANGULATION
                           , delaunay_triangulation
#endif
                           );


            // write the fields from the given partition to the main grid results
            copySubgridResultsToMain( temp_uQuantities, tempOptions.gridSize, tempOpt.uField, tempOpt, subgridList, uQuantities );
            copySubgridResultsToMain( temp_aQuantities, tempOptions.gridSize, tempOpt.aField, tempOpt, subgridList, aQuantities );
        }
    }
    else
    {
        vector<Particle_data> tempPart;
        if ( not userOptions.regionOn and not userOptions.partitionOn )   // if none of the options '--region' or '--partition' were selected, pad the box
        {
            // find the box values with padding added and find the particles in that extended region
            tempOptions.region = tempOptions.boxCoordinates;
            tempOptions.paddedBox = tempOptions.region;
            tempOptions.paddedBox.addPadding( tempOptions.paddingLength ); // now stores the padded region of interest

            if ( userOptions.periodic ) // if periodic box, translate the box
            {
                findParticlesInBox( *particlePointer, &tempPart, tempOptions );

                // update 'particlePointer' -> should point to 'tempPart'
                particlePointer->clear();   //clear the particle positions - don't need them anymore
                particlePointer = &tempPart;
            }
        }

        tempOptions.periodic = false;   // do not use the periodic translation algorithm in the "triangulation_interpolation.cpp" file


        //compute the DTFE interpolation
        DTFE_parallel( particlePointer, samples, tempOptions, uQuantities, aQuantities
#ifdef TRIANGULATION
                       , delaunay_triangulation
#endif
                       );
    }


    // compute the velocity divergence, shear or vorticity, if any
    computeDivergenceShearVorticity( userOptions.uField, userOptions.verboseLevel,
                                     userOptions.periodic, userOptions.gridSize,
                                     uQuantities );
    computeDivergenceShearVorticity( userOptions.aField, userOptions.verboseLevel,
                                     userOptions.periodic,  userOptions.gridSize,
                                     aQuantities );

}



// computes the actual Delaunay triangulation and the interpolation to grid
extern void DTFE_interpolation(vector<Particle_data> *p,
                               vector<Sample_point> &samples,
                               User_options &userOptions,
                               Quantities *uQuantities,
                               Quantities *aQuantities
#ifdef TRIANGULATION
                               , DT &delaunay_triangulation
#endif
                               );

/* Calls the function corresponding to the chosen interpolation method. */
void interpolate(vector<Particle_data> *allParticles,
                 vector<Sample_point> &samples,
                 User_options & userOptions,
                 Quantities *uQuantities,
                 Quantities *aQuantities
#ifdef TRIANGULATION
                 , DT &delaunay_triangulation
#endif
                 )
{
    if ( userOptions.DTFE )
        DTFE_interpolation( allParticles, samples, userOptions, uQuantities, aQuantities
#ifdef TRIANGULATION
                            , delaunay_triangulation
#endif
                            );
    else if ( userOptions.CIC )
        CIC_interpolation( allParticles, samples, userOptions, aQuantities );
    else if ( userOptions.TSC )
        TSC_interpolation( allParticles, samples, userOptions, aQuantities );
    else if ( userOptions.SPH )
        SPH_interpolation( allParticles, samples, userOptions, aQuantities );
    else
        throwError( "Unknow interpolation method in function 'interpolate'." );
}




/* This function splits the data in several partitions that can be used by shared memory processors to do the computations in parallel. It uses the OpenMP directives for doing so. */
void DTFE_parallel(vector<Particle_data> *allParticles,
                   vector<Sample_point> &samples,
                   User_options & userOptions,
                   Quantities *uQuantities,
                   Quantities *aQuantities
#ifdef TRIANGULATION
                   , DT &delaunay_triangulation
#endif
                   )
{
#ifndef OPEN_MP
    //directly compute the DTFE interpolation
    interpolate( allParticles, samples, userOptions, uQuantities, aQuantities
#ifdef TRIANGULATION
                 , delaunay_triangulation
#endif
                 );  // this function deletes the vector 'allParticles'
    return;
#else
    int n1 = omp_get_thread_limit(); /* global per program */
    int n2 = omp_get_max_threads();  /* per parallel region */
    int const noAvailableProcessors = ((n1 < n2)? n1 : n2);


#ifdef TRIANGULATION
    // If subpartition.cc has not been updated to allow unification of
    // delaunay_triangulation structures intended for higher level
    // usage, then openmp parallelisation is pointless.  You must add
    // code for unifying the triangulations if you wish to set
    // parallel_triangulation to 'true'. Otherwise, leave it set to 'false'.
    bool parallel_triangulation = false;
#endif

    // if only 1 processor is available, do nothing
    if ( noAvailableProcessors==1 or not samples.empty() or userOptions.redshiftConeOn
#ifdef TRIANGULATION
         or (!parallel_triangulation)
#endif
         )
    {
        interpolate( allParticles, samples, userOptions, uQuantities, aQuantities
#ifdef TRIANGULATION
                     , delaunay_triangulation
#endif
                     );
        return;
    }


    // if more than 1 processor is available, split the data in partitions
    std::vector<size_t> pGrid(NO_DIM,0); // the parallel grid
    parallelGrid( noAvailableProcessors, userOptions, &(pGrid[0]) );
    int const noProcessors = NO_DIM==2 ? pGrid[0]*pGrid[1] : pGrid[0]*pGrid[1]*pGrid[2];    //number of processors actually used (may be different from 'noAvailableProcessors')

    // reserve memory for the quantities of interest
    uQuantities->reserveMemory( &(userOptions.gridSize[0]), userOptions.uField );
    aQuantities->reserveMemory( &(userOptions.gridSize[0]), userOptions.aField );

    // define some variables to keep track of the time and particle numbers associated to each processor
    MESSAGE::Message message( userOptions.verboseLevel );
    message << "From now on only the master thread will show messages on how the computation is going. Not all threads take the same execution time, so there may be a discrepancy between the messages displayed to the user and the computations across all threads.\n\n" << MESSAGE::Flush;

    /* TODO: change to C++ style allocation instead of C style */
    size_t* processorParticles = (size_t*)malloc(sizeof(size_t)*(size_t)noProcessors);    // number of particles associated to each processor
    Real* processorTime = (Real*)malloc(sizeof(Real)*(size_t)noProcessors);    // the actual runtime of each thread

    /* OLD VERSION: These use variable length arrays, which
       are forbidden in ISO C++; see e.g.
       https://stackoverflow.com/questions/1887097/why-arent-variable-length-arrays-part-of-the-c-standard#1887178
    */

    // size_t processorParticles[noProcessors];    // number of particles associated to each processor
    //Real processorTime[noProcessors];           // the actual runtime of each thread
    size_t const noTotalParticles = allParticles->size();


        // determine the optimal grid to split the computational load
        std::vector< std::vector<size_t> > subgridList;
        std::vector< Box > subgridCoords;
        optimalPartitionSplit( *allParticles, userOptions, pGrid, &subgridList, &subgridCoords );


    // this contains the parallel section
#pragma omp parallel num_threads( noProcessors )
    {
        int const threadNo = omp_get_thread_num(); // the number of the thread - between 0 to noProcessors
        User_options tempOptions = userOptions;
        tempOptions.noProcessors = noProcessors;    // the number of parallel threads
        tempOptions.threadId = threadNo;            // the id of the thread
        tempOptions.verboseLevel = (userOptions.verboseLevel>0) ? 1 : userOptions.verboseLevel;   // the slave processors will not show runtime messages (with exception of errors and warnings)

        if ( threadNo==0 )
            tempOptions.verboseLevel = userOptions.verboseLevel;    // only the master will show runtime messages
        tempOptions.partNo = threadNo;
        tempOptions.partition.clear();
        for (int i=0; i<NO_DIM; ++i)
            tempOptions.partition.push_back( pGrid[i] );
        tempOptions.updateFullBox( userOptions.region );    // this is the new full box


        // compute the region allocated to each processor
                copySubgridInformation( &tempOptions, subgridList, subgridCoords );     // writes the size of the subgrid to tempOptions.gridSize and the boundaries of the subgrid box to tempOptions.region
        tempOptions.paddedBox = tempOptions.region;
        tempOptions.paddedBox.addPadding( tempOptions.paddingLength );


        // find the particles in the 'paddedBox'
        vector<Particle_data> particles;
        findParticlesInBox( *allParticles, &particles, tempOptions );
        processorParticles[ threadNo ] = particles.size();


        //compute the DTFE interpolation
        Quantities temp_uQuantities, temp_aQuantities; // temporary variable to store the grid interpolation for each subgrid
        interpolate( &particles, samples, tempOptions, &temp_uQuantities, &temp_aQuantities
#ifdef TRIANGULATION
                     , delaunay_triangulation
#endif
                     ); //this function deletes the vector 'particles'
        processorTime[ threadNo ] = tempOptions.totalTime;  // this is the total CPU time for all threads until this thread ended


        // write the fields from the given partition to the main grid results
        copySubgridResultsToMain( temp_uQuantities, userOptions.gridSize, tempOptions.uField, tempOptions, subgridList, uQuantities );
        copySubgridResultsToMain( temp_aQuantities, userOptions.gridSize, tempOptions.aField, tempOptions, subgridList, aQuantities );  //copy the results from each processor to a main variable result
        if ( threadNo==0 )
            message << "\nWaiting for all threads to finish the computations ... " << MESSAGE::Flush;
    }
    message << "Done.\n";
    allParticles->clear();


    // show statistics to the user about the execution in parallel
    userOptions.totalTime += maximum( processorTime, noProcessors );    // update total time by the largest value of CPU time
    approximativeThreadTime( processorTime, noProcessors );   // compute approximative CPU times for each thread
    message << "Statistics of the execution across the " << noProcessors << " threads:\n";
    for (int i=0; i<noProcessors; ++i)
        message << "\t Thread " << i << " had " << processorParticles[i] << " particles (which represent " << setprecision(4) <<  Real(processorParticles[i])/noTotalParticles*100. << "\%) and took " << processorTime[i] << " sec. \n";
    message << MESSAGE::Flush;

    free(processorTime);
    free(processorParticles);

#endif
}






//! Velocity derivative quantities
/* Computes the velocity divergence from the velocity gradient. */
inline Real velocityDivergence(Pvector<Real,noGradComp> &velGrad)
{
#if NO_DIM==2
    return velGrad[0] + velGrad[3];
#elif NO_DIM==3
    return velGrad[0] + velGrad[4] + velGrad[8];
#endif
}

/* Estimates the velocity divergence error from the rms error per gradient component.
   Assumes zero covariance. */
inline Real velocityDivergenceSigma(Real &sigma_grad_rms)
{
#if NO_DIM==2
  return sqrt(2.0) * sigma_grad_rms;
#elif NO_DIM==3
  return sqrt(3.0) * sigma_grad_rms;
#endif
}


/* Computes the velocity shear from the velocity gradient. */
Pvector<Real,noShearComp> velocityShear(Pvector<Real,noGradComp> &velGrad)
{
    Pvector<Real,noShearComp> temp;
    size_t index = 0;
    for (int i=0; i<NO_DIM-1; ++i)
        for (int j=i; j<NO_DIM; ++j)
            temp[index++] = (velGrad[i*NO_DIM+j] + velGrad[j*NO_DIM+i]) / 2.;

    // still need to subtract the trace since the shear matrix is traceless
#if NO_DIM==2
    Real trace = (temp[0]+velGrad[3]) / 2.;
    temp[0] -= trace;
#elif NO_DIM==3
    Real trace = (temp[0]+temp[3]+velGrad[8]) / 3.;
    temp[0] -= trace;
    temp[3] -= trace;
#endif
    return temp;
}


/* Computes the velocity shear squared amplitude from the velocity gradient. */
inline Real velocityShearSquared(Pvector<Real,noGradComp> &velGrad)
{
#if NO_DIM==3
    Real temp = 0.0;
    int j;

    for (int i=0; i<NO_DIM; ++i)
        for (j=0; j<NO_DIM; ++j)
          if(i != j){
            temp += velGrad[i*NO_DIM+j]*velGrad[i*NO_DIM+j];
          };
    temp *= 0.25;

    for (int i=0; i<NO_DIM; ++i){
      j = (i+1) % NO_DIM;
      temp += 0.5 * velGrad[i*NO_DIM+j] * velGrad[j*NO_DIM+i];
    };

    // loop over 0, 4, 8 -th components
    for (int i=0; i<NO_DIM*NO_DIM; i += NO_DIM+1){ // NO_DIM+1 = 4
      temp += velGrad[i]*velGrad[i] / 3.0;
      temp -= velGrad[i]*velGrad[ (i+4) % 12 ] / 3.0; // (8+4)%12 = 0
    };

    return temp;
#else
    return (1.0); // TODO: NO_DIM==2 not yet programmed; do it!
#endif
}


/* Computes the upper triangular half of the velocity vorticity from the velocity gradient. */
Pvector<Real,noVortComp> velocityVorticity(Pvector<Real,noGradComp> &velGrad)
{
    Pvector<Real,noVortComp> temp;
    size_t index = 0;
    for (int i=0; i<NO_DIM; ++i)
        for (int j=i+1; j<NO_DIM; ++j)
            temp[index++] = (velGrad[i*NO_DIM+j] - velGrad[j*NO_DIM+i]) / 2.;
    return temp;
}

/* Computes the velocity vorticity squared amplitude from the velocity gradient. */
inline Real velocityVorticitySquared(Pvector<Real,noGradComp> &velGrad)
{
    Real temp = 0.0;
    int j;
    for (int i=0; i<NO_DIM; ++i)
        for (j=0; j<NO_DIM; ++j)
          if(i != j){
            temp += velGrad[i*NO_DIM+j]*velGrad[i*NO_DIM+j];
          };
    temp *= 0.25;
    for (int i=0; i<NO_DIM; ++i){
      j = (i+1) % NO_DIM;
      temp -= 0.5 * velGrad[i*NO_DIM+j] * velGrad[j*NO_DIM+i];
    };

    return temp;
}



/* Computes the velocity IInd invariant from the velocity gradient. */
inline Real velocityIIinv(Pvector<Real,noGradComp> &velGrad)
{
  /* II: = tr(v^i_{,j})^2 - tr(v^i_{,j} ^2)
     maxima:
     load(functs);
     m : matrix([a0, a1], [a2 a3]);
     tracematrix(m)^2 - tracematrix(m.m);
     m : matrix([a0, a1, a2], [a3, a4, a5], [a6, a7, a8]);
     tracematrix(m)^2 - tracematrix(m.m);
  */
#if NO_DIM==2
  return velGrad[0] * velGrad[3]
    - velGrad[1] * velGrad[2];
#elif NO_DIM==3
  return velGrad[4] * velGrad[8]
    + velGrad[0] * velGrad[8]
    + velGrad[0] * velGrad[4]
    - velGrad[5] * velGrad[7]
    - velGrad[2] * velGrad[6]
    - velGrad[1] * velGrad[3];
#endif
}

/* Computes the velocity IInd invariant uncertainty from the velocity
   gradient and the rms error per gradient component. Assumes zero covariance. */
inline Real velocityIIinvSigma(Pvector<Real,noGradComp> &velGrad,
                               Real &sigma_grad_diag_rms,
                               Real &sigma_grad_offdiag_rms)
{
  Real sum = 0.0;
  size_t i;
#if NO_DIM==2
  /* on-diag components 0 3 */
  for (i=0; i<4; i+=3){
    sum += sigma_grad_diag_rms*sigma_grad_diag_rms *
      velGrad[i]*velGrad[i];
  };
  /* off-diag components 1 2 */
  for (i=1; i<3; i++){
    sum += sigma_grad_offdiag_rms*sigma_grad_offdiag_rms *
        velGrad[i]*velGrad[i];
  };
  return sqrt(sum);
#elif NO_DIM==3
  /* on-diag components 0 4 8 each occur twice */
  for (i=0; i<9; i+=4){
    sum += 2.0 * sigma_grad_diag_rms*sigma_grad_diag_rms *
      velGrad[i]*velGrad[i];
  };
  /* off-diag components 1 2 3 5 6 7 each occur once */
  for (i=1; i<8; i++){
    if(4!=i)
      sum += sigma_grad_offdiag_rms*sigma_grad_offdiag_rms *
        velGrad[i]*velGrad[i];
  };
  return sqrt(sum);
#endif
}


/* Computes the velocity IIIrd invariant from the velocity gradient. */
Real velocityIIIinv(Pvector<Real,noGradComp> &velGrad)
{
#if NO_DIM==2
  /* det(v^i_{,j}) */
  return velGrad[0] * velGrad[3]
    - velGrad[1] * velGrad[2];
#elif NO_DIM==3
    /* det(v^i_{,j}) */
  return velGrad[0] * velGrad[4] * velGrad[8]
    + velGrad[1] * velGrad[5]  * velGrad[6]
    + velGrad[2] * velGrad[3]  * velGrad[7]
    - velGrad[0] * velGrad[5]  * velGrad[7]
    - velGrad[1] * velGrad[3]  * velGrad[8]
    - velGrad[2] * velGrad[4]  * velGrad[6];
#else
  return 0;
#endif
}


/* Computes the velocity IInd invariant from the velocity gradient. */
inline Real velocityQ(Pvector<Real,noGradComp> &velGrad)
{
  /* Q \equiv 2 II - 2/3 I^2 \equiv 2 II - 2/3 divergence^2
  */
  Real div;
  div = velocityDivergence(velGrad);
  return (2.0 * velocityIIinv(velGrad) - (2.0/3.0)*div*div);
}


/* Computes the kinematical backreaction uncertainty from the
   velocity gradient and the rms error per gradient
   component. Assumes zero covariance. */
inline Real velocityQSigma(Pvector<Real,noGradComp> &velGrad,
                           Real &sigma_grad_diag_rms,
                           Real &sigma_grad_offdiag_rms)
{
  Real div, sig_div, sig_II;

  /* possible TODO: if this is a cpu time bottleneck, then
     re-use already existing calculations of the following
     three parameters */
  div = velocityDivergence(velGrad);
  sig_div = velocityDivergenceSigma(sigma_grad_diag_rms);
  sig_II = velocityIIinvSigma(velGrad, sigma_grad_diag_rms,
                              sigma_grad_offdiag_rms);

  return sqrt(4.0*sig_II*sig_II +
              (16.0/9.0)* sig_div*sig_div * div*div);
}


/* Periodic case: compute approximate estimate of uncertainties based
    on Stokes' theorem in one given direction. For the periodic case,
    varying x^i over the full range of j, i.e. holding x^{k \not= j} fixed,
    gives a closed straight loop. Stokes' theorem becomes the 1D
    Fundamental theorem of calculus, applied to S^1, or equivalently,
    a finite interval with identified boundaries. The total gradient
    should be zero if no numerical error occurs. Thus, we estimate the
    error using all possible choices of S^1 loops for a given gradient
    component. See Roukema et al (in preparation, 2016) for details.

    The uncertainty estimate is \sum_{i,k} \sum_j v^i_{,j}/sqrt(N_{v^i_{,j}}).
*/

inline double estimateSigmaGradientOneDirec(std::vector< Pvector<Real,noGradComp> > gradient,
                                            int const verboseLevel,
                                            int const j_S1, /* direction of integration */
                                            int const i_vector, /* vector component */
                                            std::vector<size_t> grid){
      // NO_DIM==2 not yet programmed
#if NO_DIM==3
  size_t ip, jp, kp;
  size_t j_S1_other1, j_S1_other2;
  size_t index;
  size_t i_grad_tensor_component;
  // closed straight loops are orthogonal to the faces
  // estimate of standard error in 1D gradient component at a grid
  // point:
  double sum_grad;

  double sigma_grad_list;
  double *sigma_grad_list_sq; /* square values */
  double sigma_grad_rms; /* rms error per grid interval */


  /* find the two directions which are *not* the j_S1'th direction */
  switch (j_S1)
    {
    case 0 :
      j_S1_other1 = 1;
      j_S1_other2 = 2;
      break;
    case 1 :
      j_S1_other1 = 0;
      j_S1_other2 = 2;
      break;
    case 2 :
      j_S1_other1 = 0;
      j_S1_other2 = 1;
      break;
    default:
      throwError("Error calling estimateSigmaGradientOneDirec. j_S1 value invalid.");
      break;
    };

  /* set the correct component of the velocity gradient tensor */
  /* [for testing the tensor convention, see case 1 of
     tests/test_vgrad_DTFE.cpp] */

  i_grad_tensor_component = i_vector *3 + j_S1;

  sigma_grad_list_sq = (double*)malloc(sizeof(double) *grid[j_S1_other1] *grid[j_S1_other2]);

  /* outer two loops: analyse each S1 loop in the j_S1'th direction */
  for(ip=0; ip<grid[j_S1_other1]; ip++)
    for (kp=0; kp<grid[j_S1_other2]; kp++){
      /* sum the gradients in a single loop */
      sum_grad = 0.0;
      for (jp=0; jp<grid[j_S1]; jp++){
        /* the indexing must match the choice of the three integration directions */
        switch (j_S1)
          {
          case 0 :
            index = jp*grid[1]*grid[2] + ip*grid[2] + kp;
            break;
          case 1 :
            index = ip*grid[1]*grid[2] + jp*grid[2] + kp;
            break;
          case 2 :
            index = ip*grid[1]*grid[2] + kp*grid[2] + jp;
            break;
          default:
            throwError("Error calling estimateSigmaGradientOneDirec. j_S1 value invalid.");
            break;
          };

        sum_grad += gradient[index][i_grad_tensor_component];
      }; /* for (jp=0; jp<grid[j_S1]; jp++) */

      /* find expected rms grid interval in j_S1 direction, assuming each
         provides an independent gaussian estimate of zero with the
         same gaussian width; avoid dividing by zero in the
         unrealistic case of a grid size of 1.
       */
      /* The "fabs" is for aesthetic purposes only: */
      sigma_grad_list =
        fabs(sum_grad) /sqrt(fmax(1.0, grid[j_S1]-1.0));
      sigma_grad_list_sq[ ip*grid[j_S1_other2] + kp ] =
        sigma_grad_list * sigma_grad_list ;
    }; /*     for (kp=0; kp<grid[j_S1_other2]; kp++) */
       /*   for(ip=0; ip<grid[j_S1_other1]; ip++) */

  /* uncertainty in sum */
  sigma_grad_rms =
    sqrt( gsl_stats_mean(sigma_grad_list_sq,1,
                         grid[j_S1_other1]*grid[j_S1_other2]) );

  free(sigma_grad_list_sq);

  return sigma_grad_rms;

#else
  return (1.0);
#endif
}


/* periodic case: compute approximate estimate of
      uncertainties based on Stokes' theorem \sum v^x,x
      /sqrt(N_x), ... , \sum v^z,z / sqrt(N_z)
      TODO: write more elegantly, with less repetition
   */
inline Real estimateSigmaGradient(std::vector< Pvector<Real,noGradComp> > gradient,
                                  int const verboseLevel,
                                  int const use_diag, /* yes= 1, no= 0 */
                                  std::vector<size_t> grid){
      // NO_DIM==2 not yet programmed
#if NO_DIM==3
      // closed straight loops are orthogonal to the faces
      // estimate of standard error in 1D gradient component at a grid
      // point:
      double sigma_grad[NO_DIM*NO_DIM];
      double sigma_grad_mean;
      double sigma_grad_stddev;

      size_t ip, jp;
      int i_directions = -1;
      int N_directions;

      Real sigma_grad_rms;

      /* size_t i_dim; */

      if(use_diag){
        // i'th direction
        sigma_grad[0] =
          estimateSigmaGradientOneDirec(gradient, verboseLevel, 0, 0, grid);

        // j'th direction
        sigma_grad[1] =
          estimateSigmaGradientOneDirec(gradient, verboseLevel, 1, 1, grid);

        // k'th direction
        sigma_grad[2] =
          estimateSigmaGradientOneDirec(gradient, verboseLevel, 2, 2, grid);

        // combine the three directions
        sigma_grad_mean = gsl_stats_mean(sigma_grad,1, NO_DIM);

        sigma_grad_stddev =
          sqrt(gsl_stats_variance(sigma_grad,1, NO_DIM));

      }else{
        /* off-diag uncertainties */
        for(ip=0; ip<3; ip++)
          for(jp=0; jp<3; jp++){
            if(ip != jp){
              i_directions++;
              sigma_grad[i_directions] =
                estimateSigmaGradientOneDirec(gradient, verboseLevel, ip, jp, grid);
            };
          };
        N_directions = i_directions + 1;

        // combine the eight directions
        if(6 != N_directions){
          throwError("estimateSigmaGradient - wrong number of off-diag directions.");
        };
        sigma_grad_mean = gsl_stats_mean(sigma_grad,1, N_directions);

        sigma_grad_stddev =
          sqrt(gsl_stats_variance(sigma_grad,1, N_directions));
      };

      MESSAGE::Message message(verboseLevel);
      message << "Gradient error analysis:\n" << MESSAGE::Flush;
      message << "sigma_grad_mean:  " << sigma_grad_mean <<
        "\n" << MESSAGE::Flush;
      message <<  "sigma_grad_stddev:   " << sigma_grad_stddev <<
        "\n" << MESSAGE::Flush;

      /* calculate rms uncertainty (restore mean square) */
      sigma_grad_rms = sqrt(sigma_grad_stddev*sigma_grad_stddev +
                            sigma_grad_mean*sigma_grad_mean);

      message <<  "sigma_grad_rms = " << sigma_grad_rms <<
        " will be used.\n" << MESSAGE::Flush;
      return sigma_grad_rms;
#else
      return (1.0);
#endif
}

/* global Q as a control for zero-boundary Newtonian simulations */
inline int estimateQGlobal(std::vector<Real> div,
                           std::vector<Real> IIinv,
                           std::vector<Real> div_sigma,
                           std::vector<Real> IIinv_sigma,
                           int const verboseLevel,
                           std::vector<size_t> grid,
                           Real &Q_global,
                           Real &Q_global_sigma)
{

  // NO_DIM==2 not yet programmed
#if NO_DIM==3
  size_t ip, jp, kp;
  size_t index;
  Real IIinv_sig_global_sq = 0.0;
  Real div_global = 0.0;
  Real div_sig_global_sq;
  Real Nijk = (Real)(grid[0] * grid[1] * grid[2]);

  Real IIinv_global = 0.0;

  Q_global = 0.0;

  for (ip=0; ip<grid[0]; ip++)
    for (jp=0; jp<grid[1]; jp++)
      for (kp=0; kp<grid[2]; kp++){
        index = ip*grid[1]*grid[2] + jp*grid[2] + kp;
        Q_global += 2.0* IIinv[index];
        div_global += div[index];
        IIinv_sig_global_sq += IIinv_sigma[index]
          * IIinv_sigma[index];
        IIinv_global += IIinv[index];
      }; /* for (ip=0; ip<grid[0]; ip++) */
  /* divide by volume, i.e. by number of grid cells Nijk */
  Q_global /= Nijk;
  div_global /= Nijk;
  IIinv_global /= Nijk;
  Q_global -= (2.0/3.0) * div_global*div_global;

  /* uncertainties: sum of square uncertainties */
  div_sig_global_sq = div_sigma[0]
    * div_sigma[0] * Nijk; /* since assumed equal everywhere */

  /* as above, assume Gaussian errors and zero covariance
     between grid locations; see Roukema et al (2016; in preparation) */
  Q_global_sigma = (2.0/ Nijk * sqrt( IIinv_sig_global_sq +
                                     (4.0/9.0) * div_sig_global_sq
                                      *div_global*div_global) );
  MESSAGE::Message message( verboseLevel );
  message <<  "div_global = " << div_global <<
    " \\pm " << sqrt(div_sig_global_sq)/Nijk << " (1 \\sigma)\n" << MESSAGE::Flush;

  message <<  "II_global = " << IIinv_global <<
    " \\pm " << sqrt(IIinv_sig_global_sq)/Nijk << " (1 \\sigma)\n" << MESSAGE::Flush;

  message <<  "Q_global = " << Q_global <<
    " \\pm " << Q_global_sigma << " (1 \\sigma)\n" << MESSAGE::Flush;
#endif
  return 0;
}

/* This function computes the velocity divergence, shear and vorticity. */
void computeDivergenceShearVorticity(Field &fields,
                                     int const verboseLevel,
                                     bool const periodic,
                                     std::vector<size_t> grid,
                                     Quantities *q)
{
  /* default values: the S^1 method does not apply in non-periodic cases */
    Real sigma_grad_diag_rms = 9e9;
    Real sigma_grad_offdiag_rms = 9e9;
    Real Q_global, Q_global_sigma;
    if ( q->velocity_gradient.empty() ) return;

    if(periodic){
      sigma_grad_diag_rms =
        estimateSigmaGradient(q->velocity_gradient,
                              verboseLevel, 1,
                              grid);
      sigma_grad_offdiag_rms =
        estimateSigmaGradient(q->velocity_gradient,
                              verboseLevel, 0,
                              grid);
    }; /* if(periodic) */




    // compute the velocity divergence
    if ( fields.velocity_divergence )
    {
        q->velocity_divergence.reserve( q->velocity_gradient.size() );
        for (std::vector< Pvector<Real,noGradComp> >::iterator it=q->velocity_gradient.begin(); it!=q->velocity_gradient.end(); ++it )
            q->velocity_divergence.push_back( velocityDivergence(*it) );

        /* should only push back a single value to the velocity_divergence_sigma vector */
        if(periodic)
          q->velocity_divergence_sigma.push_back( velocityDivergenceSigma(sigma_grad_diag_rms) );

//        for (int i=0; i<q->velocity_divergence.size(); ++i)
//            if ( not boost::math::isfinite(q->velocity_divergence[i]) )
//            {
//                q->velocity_divergence[i] = Real(0.);
//                std::cout << "<<< Found non-numerical value in velocity divergence at array index " << i << ". The value of the velocity divergence at this grid point will be set to 0." << std::flush;
//            }
    }

    // compute the velocity shear
    if ( fields.velocity_shear )
    {
        q->velocity_shear.reserve( q->velocity_gradient.size() );
        for (std::vector< Pvector<Real,noGradComp> >::iterator it=q->velocity_gradient.begin(); it!=q->velocity_gradient.end(); ++it )
            q->velocity_shear.push_back( velocityShear(*it) );
    }

    // compute the velocity vorticity
    if ( fields.velocity_vorticity )
    {
        q->velocity_vorticity.reserve( q->velocity_gradient.size() );
        for (std::vector< Pvector<Real,noGradComp> >::iterator it=q->velocity_gradient.begin(); it!=q->velocity_gradient.end(); ++it )
            q->velocity_vorticity.push_back( velocityVorticity(*it) );
    }

    // compute the velocity IInd invariant
    if ( fields.velocity_IIinv )
    {
        q->velocity_IIinv.reserve( q->velocity_gradient.size() );
        for (std::vector< Pvector<Real,noGradComp> >::iterator it=q->velocity_gradient.begin(); it!=q->velocity_gradient.end(); ++it ){
            q->velocity_IIinv.push_back( velocityIIinv(*it) );
            if(periodic)
              q->velocity_IIinv_sigma.push_back( velocityIIinvSigma(*it, sigma_grad_diag_rms, sigma_grad_offdiag_rms) );
        };
    }

    // compute the kinematical backreaction, shear squared scalar, vorticity squared scalar
    if ( fields.velocity_Q )
    {
        q->velocity_Q.reserve( q->velocity_gradient.size() );
        for (std::vector< Pvector<Real,noGradComp> >::iterator it=q->velocity_gradient.begin(); it!=q->velocity_gradient.end(); ++it ){
            q->velocity_Q.push_back( velocityQ(*it) );
            if(periodic)
              q->velocity_Q_sigma.push_back( velocityQSigma(*it, sigma_grad_diag_rms, sigma_grad_offdiag_rms) );
        };

        // also compute the velocity shear squared (directly)
        q->velocity_shear_squared.reserve( q->velocity_gradient.size() );
        for (std::vector< Pvector<Real,noGradComp> >::iterator it=q->velocity_gradient.begin(); it!=q->velocity_gradient.end(); ++it )
            q->velocity_shear_squared.push_back( velocityShearSquared(*it) );

        // also compute the velocity vorticity squared (directly)
        q->velocity_vorticity_squared.reserve( q->velocity_gradient.size() );
        for (std::vector< Pvector<Real,noGradComp> >::iterator it=q->velocity_gradient.begin(); it!=q->velocity_gradient.end(); ++it )
            q->velocity_vorticity_squared.push_back( velocityVorticitySquared(*it) );

    }

    // periodic case:
    // if the divergence and IInd invariant have already been calculated,
    // then calculate the global kinematical backreaction and its uncertainty
    if ( periodic and fields.velocity_divergence and fields.velocity_IIinv )
    {
      estimateQGlobal(q->velocity_divergence,
                      q->velocity_IIinv,
                      q->velocity_divergence_sigma,
                      q->velocity_IIinv_sigma,
                      verboseLevel,
                      grid,
                      Q_global,
                      Q_global_sigma);
    };


    // compute the velocity IIIrd invariant
    if ( fields.velocity_IIIinv )
    {
        q->velocity_IIIinv.reserve( q->velocity_gradient.size() );
        for (std::vector< Pvector<Real,noGradComp> >::iterator it=q->velocity_gradient.begin(); it!=q->velocity_gradient.end(); ++it )
            q->velocity_IIIinv.push_back( velocityIIIinv(*it) );
    }



    if ( not fields.velocity_gradient )
        q->velocity_gradient.clear();
}





// intialize the static members
#ifndef VELOCITY
Pvector<Real,noVelComp> Data_structure::_velocity = Pvector<Real,noVelComp>::zero();
#endif
#ifndef SCALAR
Pvector<Real,noScalarComp> Data_structure::_scalar = Pvector<Real,noScalarComp>::zero();
#endif
