/*
 *  Copyright (c) 2011       Marius Cautun
 *
 *                           Kapteyn Astronomical Institute
 *                           University of Groningen, the Netherlands
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef DTFE_HEADER
#define DTFE_HEADER

#include <vector>

#include "define.h"
#include "user_options.h"
#include "particle_data.h"
#include "quantities.h"


// Computes the grid interpolation
void DTFE(std::vector<Particle_data> *allParticles,
          std::vector<Sample_point> &samples,
          User_options &userOptions,
          Quantities *uQuantities,
          Quantities *aQuantities);


// section used if you need to access the Delaunay triangulations
#ifdef TRIANGULATION
// include the headers
#if NO_DIM==2
#include "CGAL_triangulation/CGAL_include_2D.h"
#elif NO_DIM==3
#include "CGAL_triangulation/CGAL_include_3D.h"
#endif
#include "math_functions.h"

// Computes the grid interpolation (no partition options available as well as no parallel threads)
void DTFE(std::vector<Particle_data> *allParticles,
          std::vector<Sample_point> &samples,
          User_options &userOptions,
          Quantities *uQuantities,
          Quantities *aQuantities,
          DT &delaunay_triangulation);


#endif


int DTFE_cplusplus_wrapper(/* INPUTS */
                           int64_t *noParticles_in,
                           double  *boxsize,
                           int32_t *dtfe_averaging_method,
                           int32_t *dtfe_n_gridsize,
                           double  *xp_in, /* positions */
                           double  *vp_in, /* velocities */
                           double  *mp_in, /* masses */
                           /* OUTPUTS */
                           User_options *userOptions,
                           Quantities *aQuantities
                           );

extern "C" {
void dtfe_f90_wrapper_(/* INPUTS */
                     int64_t *noParticles_in,
                     double  *boxsize,
                     int64_t *dtfe_averaging_method,
                     int64_t *dtfe_n_gridsize,
                     double  **xp_in, /* positions */
                     double  **vp_in, /* velocities */
                     double  **mp_in, /* masses */
                     /* OUTPUTS */
                     /* These arrays must be allocated (and later
                        freed) by the calling routine*/
                     double  **dtfe_velocity, /* DT velocity Estimate */
                     double  **divergence, /* of vel gradient */
                     double  **IIinv, /* second invariant of vel gradient */
                     double  **IIIinv, /* third invariant of vel gradient */
                     double  **Q_kin_backreaction, /* kinematical backreaction map */
                     double  **Q_sigma, /* kinematical backreaction map */
                     double  **shear, /* scalar shear squared map */
                     double  **vorticity, /* scalar vorticity squared map */
                     double  **density /* density map */
                     );
}

#endif
