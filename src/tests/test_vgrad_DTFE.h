/*
   test_vgrad_DTFE - generate analytical density and velocity gradient fields for testing DTFE - header file

   Copyright (C) B. Roukema 2016, GPLv3 or later

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

/*
#define TEST_DTFE_NUM_PARTICLES (128*128*128)
*/
/*
#define TEST_DTFE_NUM_PARTICLES (64*64*64)
*/
/*

#define TEST_DTFE_NUM_PARTICLES 32768
#define TEST_DTFE_NUM_PARTICLES 4096
*/

#define TEST_DTFE_BOXSIZE 1e5

/* model types (const scalings not shown)
   0 : (sin x, sin y, sin z)
   1 : (sin y, sin z, sin x)
*/
#define TEST_DTFE_MODEL_MIN 0
#define TEST_DTFE_MODEL_MAX 1
#define TEST_DTFE_NXYZ  64
#define TEST_DTFE_NONZERO_GRAD_COMPONENTS 3
#define TEST_DTFE_AMPLITUDE 1.0

#define TEST_DTFE_AVERAGING_METHOD 1

/* obsolete
#define TEST_DTFE_N_GRIDSIZE 64
*/

#define TEST_DTFE_N_GRIDSIZE_MIN 8
/* #define TEST_DTFE_N_GRIDSIZE_STEP 8 */
#define TEST_DTFE_N_GRIDSIZE_FACTOR 2
#define TEST_DTFE_N_GRIDSIZE_MAX 16

#define TEST_DTFE_NUM_PARTICLES (128*128*128)

/*
#define TEST_DTFE_N_GRIDSIZE_STEP 20
#define TEST_DTFE_N_GRIDSIZE_MAX 200
*/

/* define this to generate new particles for every
   test_dtfe_n_gridsize value */
#define REGENERATE_PARTICLES 1
